<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>



<%@include file="/WEB-INF/jsp/comun/header.jsp" %>
<%@include file="/WEB-INF/jsp/comun/menu.jsp" %>
<%@include file="/WEB-INF/jsp/comun/urls.jsp" %>

<form action="${urlEstacionamiento}" method="POST">
    


<div class="container-fluid text-center">    
  <div class="row content">

      <a name="arriba"></a>

    <%--  Centro   --%>
    <div class="col-sm-6 text-left"   > 
        
        <%--  Info 1er estacionamiento   --%>
            <div class="panel panel-success">
              <div class="panel-heading"><b> Maule </b></div>
                <div class="panel-body">
                    <span class="glyphicon glyphicon-info-sign"></span> Informaci�n : EStacionamiento con acceso a Aeropuerto con rutas nacionales e internacionales.  <br>
                    <span class="glyphicon glyphicon-road"></span> Ubicaci�n        : Ruta 68, Pudahuel, Lo Prado <br>
                    <span class="glyphicon glyphicon-usd"></span> Valor             : 1500
                    <br>
                    <br>
                </div>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3234.0094550393314!2d-70.72874557157998!3d-33.455216434430916!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x966288fff1231003%3A0x95b593ce99322d0a!2sRuta+68!5e0!3m2!1ses!2scl!4v1512352287580" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <a href="#arriba">Subir</a>
            <br>
            
            <br>
     
    <%--  Info 2da estacionamiento   --%>
            <div class="panel panel-info">
                <div class="panel-heading"><b> Castro </b></div>
                <div class="panel-body">
                    <span class="glyphicon glyphicon-info-sign"></span> Informaci�n : Estacionamiento cerca de las  comunas aleda�as de El Monte, Maip�, Lo Pinto.  <br>
                    <span class="glyphicon glyphicon-road"></span> Ubicaci�n        : Ruta del Sol - Ricardo Ayala, Maip�<br>
                    <span class="glyphicon glyphicon-usd"></span> Valor             : 2000
                    <br>
                    <br>
                </div>
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3326.5089700453964!2d-70.75767338425527!3d-33.51415030817149!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662dcd0557ec0a5%3A0xdacb15b94c8e3d33!2sRuta+del+Sol!5e0!3m2!1ses!2scl!4v1512352447137" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <a href="#arriba">Subir</a>
            <br>
            
            <br>
            
            
            
            <%--  Info 3era estacionamiento   --%>
            <div class="panel panel-warning">
                <div class="panel-heading"><b> Puerto Montt </b></div>
                <div class="panel-body">
                    <span class="glyphicon glyphicon-info-sign"></span> Informaci�n : Estacionamiento Centro de Santiago hasta Rancagua.<br>
                    <span class="glyphicon glyphicon-road"></span> Ubicaci�n        :  Acceso Sur, Puente Alto, Regi�n Metropolitana <br>
                    <span class="glyphicon glyphicon-usd"></span> Valor             : 2500
                    <br>
                    <br>
                </div>
                 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3321.8779866727436!2d-70.63244268425241!3d-33.634405514364005!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662d8225731c943%3A0xc025fbabaebe3b46!2sUnnamed+Road%2C+San+Bernardo%2C+Regi%C3%B3n+Metropolitana!5e0!3m2!1ses!2scl!4v1512352635921" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <a href="#arriba">Subir</a>
            <br>
            
            <br>
            
    </div>
  </div>
</div>

            </form>

      
<%@include file="/WEB-INF/jsp/comun/footer.jsp" %>