<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- Se referencia paginas, debe ser en este orden, sino se descuadrara XD  -->

<%@include file="/WEB-INF/jsp/comun/header.jsp" %>
<%@include file="/WEB-INF/jsp/comun/menu.jsp" %>
<%@include file="/WEB-INF/jsp/comun/urls.jsp" %>


<!-- COMIENZO DEL CONTENIDO DE CENTRAL -->
  
      <!-- FORMULARIO -->
<form action="${urlInicio}" method="POST" >
      
    <c:if test="${not empty alerta}">
        <h3 style=""> ALERTA!: 
        <c:out value="${alerta}" escapeXml="true" />
        
        </h3>
    </c:if>  
    
    <c:if test="${not empty alerta}">
        <h3 style=""> ALERTA!: 
        <c:out value="${alerta}" escapeXml="true" />
        
        </h3>
    </c:if> 
    
    <c:if test="${not empty error}">
        <h3 style=""> ERROR!: 
        <c:out value="${error}" escapeXml="true" />
        
        </h3>
    </c:if>  
    
    <c:if test="${not empty alertad}">
        <h3 style=""> ALERTA!: 
        <c:out value="${alertad}" escapeXml="true" />
        
        </h3>
    </c:if>

    <div>
    
    <label class="col-sm-2 control-label"> Rut </label>
    <div class="col-sm-10">
        <input type="number" name="txt_rut" id="txt_rut" class="form-control" placeholder="Ingrese Rut"   >
    </div>
     
    <label class="col-sm-2 control-label"> Nombre </label>
    <div class="col-sm-10">
        <input type="text" name="txt_nombre" id="txt_nombre" class="form-control" placeholder="Ingrese Nombre"   >
    </div>
      
    <label class="col-sm-2 control-label" margin-top= "10px"> Telefono </label>
    <div class="col-sm-10" margin-top= "10px">
        <input type="number" name="txt_telefono" id="txt_telefono" class="form-control" placeholder="Ingrese Telefono"  >
    </div>
      
    <label class="col-sm-2 control-label" margin-top= "10px"> Email </label>
    <div class="col-sm-10" margin-top= "10px">
        <input type="text" name="txt_email" id="txt_email" class="form-control" placeholder="Ingrese Email"   >
    </div>
      
      
    <div class="col-md-4">
      Opciones de Pago  
      <div class="container">
          <div class="radio">
            <label><input type="radio" name="RbOpPago" value="Transferencia"> Transferencia </label>
          </div>
          <div class="radio">
            <label><input type="radio" name="RbOpPago" value="OnLine"> Pagon en Linea</label>
          </div>
          <div class="radio">
            <label><input type="radio" name="RbOpPago" value="OrdenCompra"> Orden de compra</label>
          </div>
      </div>
    </div>
      
    <div class="col-md-4">
      Opciones de Envio de Boleta  
      
     
      
      <div class="container">
          <div class="radio">
            <label><input type="radio" name="RbOpEnvio" value="Correo"> Correo Electronico </label>
          </div>
          <div class="radio">
            <label><input type="radio" name="RbOpEnvio" value="Direccion"> Direcci�n Particular</label>
          </div>
      </div>
    </div>
      
    <!-- TERMINO DEL FORMULARIO  -->
    
    <!-- Seleccion de estacionamiento -->
    
    
    <div class="col-sm-10">
         <p>Seleccione Estacionamiento antes de cargar sus datos personales</p>   
    </div>
    
   
    
    <div class="col-sm-10">
        <select class="form-control" name="cmb_estacionamiento" id="cmb_estacionamiento">
            <option value="0">Estacionamientos</option>
            <option value="1">Maule</option>
            <option value="2">Castro</option>
            <option value="3">Puerto Montt</option>
        </select> 
    </div>
    <div class="col-sm-2">
        <button type="submit" name="btn_agregar" id="btn_agregar" class="form-control" > <span class="glyphicon glyphicon-plus-sign"></span> Agregar</button>
   
    </div>
    
    </BR>
        </BR>    
    
        
    </div>
    <!-- Fin de Seleccion de estacionamiento -->
    
    <!-- Tabla con pago a realizar -->
    
<c:if test="${alertaList}">
    <table class="table table-bordered"> 
                <thead>
                    <tr>
                        <th>Cantidad</th>
                        <th>ID Estacionamiento</th>
                        <th>Monto</th>
                        <th>Estacionamiento</th> 
                        <th>Quitar</th> 
                    </tr>
                </thead>
                <tbody>
                <c:forEach items="${d_lista}" var="d" >
                    <tr>
                        <td><c:out value="${d.cantidad}" escapeXml="true" /></a></td>
                        <td><input value="<c:out value="${d.idEstacionamiento}" escapeXml="true" />" name="txtIdEst" id="txtIdEst" readonly="true" class="col-der" >  
                        <td><c:out value="${d.montoP}" escapeXml="true" /></td>
                        <td><c:out value="${d.nomEstacionamiento}" escapeXml="true" /></td>
                        <td><button type="submit" name="btn_quitar" id="btn_quitar" class="form-control" > <span class="glyphicon glyphicon-minus"></span></button></td>
                    </tr>
                  </c:forEach> 
                </tbody>
        </table>

        
    <!-- Monto a pagar y boton --> 
    
    <div >
        <div class="col-sm-5">
        <label class="col-iz-vo" id="lblMonto"> Monto a pagar </label>
        </div>    
        
        <div class="col-sm-5">
        <input type="text" name="lblMontoP" id="<c:out value="${monto}" escapeXml="true" />" class="col-der-vo" value= "<c:out value="${monto}" escapeXml="true" />" readonly="true" >
        </div> 
        
       <!--  <label class="col-sm-2 control-label" id="lblMontoP" name="lblMontoP"  > <c:out value="${monto}" escapeXml="true" /></label>--> 
      
        <div class="col-sm-2">
            <button type="submit" name="btn_pagar" id="btn_pagar" class="form-control" > <span class="glyphicon glyphicon-plus-sign"></span>Pagar</button>
        </div> 
         
    </div>
    
   </c:if> 
    
    
    
</form>
    <!-- TERMINO DEL CONTENIDO DE CENTRAL -->
      


<%@include file="/WEB-INF/jsp/comun/footer.jsp" %>