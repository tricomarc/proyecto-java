<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>



<%@include file="/WEB-INF/jsp/comun/header.jsp" %>
<%@include file="/WEB-INF/jsp/comun/menu.jsp" %>
<%@include file="/WEB-INF/jsp/comun/urls.jsp" %>



   
      <section>
          <form action="${urlPagos}" method="POST">
              
              <h3>B�squeda de Pagos</h3>
              
              <div>
                <div class="col-sm-5">
                    
                  <select class="form-control" name="cmb_filtro" id="cmb_filtro">
                        <option value="1">Rut Cliente</option>
                        <option value="2">N� Boucher</option>
                  </select>  
                </div>    
                  
                <div class="col-sm-5">
                    <input type="text" name="txtRut" id="txtRut" class="form-control" placeholder="Ingrese dato" required="">
                
                </div> 

                <div class="col-sm-2">
                     <button type="submit" name="btn_buscar" id="btn_buscar" class="form-control" > <span class="glyphicon glyphicon-plus-sign"></span> Buscar</button>
                </div>
                  
              </div>    
              
              <br>
              <c:if test="${alertList}">
                  <table class="table table-bordered">
                    <thead>
                        <tr>
                          <th>Estacionamiento</th>
                          <th>Total</th>
                          <th>Rut Cliente</th>
                          <th>N�mero Voucher</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${lista}" var="e">
                            <tr>
                                <td><c:out value="${e.nomEstacionamiento}" escapeXml="true"/></a></td>
                                <td><c:out value="${e.montoP}" escapeXml="true"/></a></td>
                                <td><c:out value="${e.idPago.rutCliente}" escapeXml="true"/></a></td>
                                <td><c:out value="${e.idPago.numeroBoucher}" escapeXml="true"/></a></td>  
                            </tr>
                        </c:forEach>
                    </tbody>
                 </table>
              </c:if>
          </form>
      </section>
      
<%@include file="/WEB-INF/jsp/comun/footer.jsp" %>