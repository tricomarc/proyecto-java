<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:url var="urlPagos" value="/pago"/>
<c:url var="urlVoucher" value="/voucher"/>
<c:url var="urlInicio"     value="/inicio"/>
<c:url var="urlEstacionamiento"  value="/estacionamiento"/>


<!-- COMIENZO DEL CONTENIDO DE LA IZQUIERDA, MENU -->

<div class="col-sm-2 sidenav">
     
    <BR>
    <BR>
    <BR>
    <BR>
    
    <ul>
          <li> 
              <a href="${urlInicio}">  Inicio</a></li>
            <BR>
          <li> <a href="${urlPagos}">Ver Pagos / Compras</a> </li>
            <BR>
          <li> <a href="${urlVoucher}">Ver Ultimo Boucher</a> </li>
            <BR>
          <li> <a href="${urlEstacionamiento}">Ver Estacionamientos</a>  </li>
            <BR>
      </ul>
      
   
      
</div>

<!-- FIN DEL CONTENIDO DE LA IZQUIERDA, MENU -->

<!-- COMIENZO DEL CONTENIDO CENTRAL -->

<div class="col-sm-9">