/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dej.autopark.model;

import java.util.Objects;

/**
 *
 * @author IBMSEGURIDAD01
 */
public class Pago {
    
    private int numeroBoucher;
    private String opcionEnvio;
    private String opcionPago;
    
    private int rutCliente;
    private String nombreCliente;
    private int telefono;
    private String email;
    
    private int totalPago;

    public Pago() {
    }

    public int getNumeroBoucher() {
        return numeroBoucher;
    }

    public void setNumeroBoucher(int numeroBoucher) {
        this.numeroBoucher = numeroBoucher;
    }

    public String getOpcionEnvio() {
        return opcionEnvio;
    }

    public void setOpcionEnvio(String opcionEnvio) {
        this.opcionEnvio = opcionEnvio;
    }

    public String getOpcionPago() {
        return opcionPago;
    }

    public void setOpcionPago(String opcionPago) {
        this.opcionPago = opcionPago;
    }

    public int getRutCliente() {
        return rutCliente;
    }

    public void setRutCliente(int rutCliente) {
        this.rutCliente = rutCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTotalPago() {
        return totalPago;
    }

    public void setTotalPago(int totalPago) {
        this.totalPago = totalPago;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.numeroBoucher;
        hash = 79 * hash + Objects.hashCode(this.opcionEnvio);
        hash = 79 * hash + Objects.hashCode(this.opcionPago);
        hash = 79 * hash + this.rutCliente;
        hash = 79 * hash + Objects.hashCode(this.nombreCliente);
        hash = 79 * hash + this.telefono;
        hash = 79 * hash + Objects.hashCode(this.email);
        hash = 79 * hash + this.totalPago;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pago other = (Pago) obj;
        if (this.numeroBoucher != other.numeroBoucher) {
            return false;
        }
        if (this.rutCliente != other.rutCliente) {
            return false;
        }
        if (this.telefono != other.telefono) {
            return false;
        }
        if (this.totalPago != other.totalPago) {
            return false;
        }
        if (!Objects.equals(this.opcionEnvio, other.opcionEnvio)) {
            return false;
        }
        if (!Objects.equals(this.opcionPago, other.opcionPago)) {
            return false;
        }
        if (!Objects.equals(this.nombreCliente, other.nombreCliente)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        return true;
    }

    
    
    
    
    
}
