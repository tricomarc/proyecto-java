/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dej.autopark.model;

import java.util.Objects;

/**
 *
 * @author Raniela
 */
public class Detalle {
    
    private int id;
    private Pago idPago;
    private int idEstacionamiento;
    private String nomEstacionamiento;
    private int cantidad;
    // es lo mismo que el id
    private int numeroTick;
    private int montoP;
    

    public int getIdEstacionamiento() {
        return idEstacionamiento;
    }

    public void setIdEstacionamiento(int idEstacionamiento) {
        this.idEstacionamiento = idEstacionamiento;
    }

    public String getNomEstacionamiento() {
        return nomEstacionamiento;
    }

    public void setNomEstacionamiento(String nomEstacionamiento) {
        this.nomEstacionamiento = nomEstacionamiento;
    }
    
    
    public int getMontoP() {
        return montoP;
    }

    public void setMontoP(int montoP) {
        this.montoP = montoP;
    }

    public int getNumeroTick() {
        return numeroTick;
    }

    public void setNumeroTick(int numeroTick) {
        this.numeroTick = numeroTick;
    }
   

    public Detalle() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Pago getIdPago() {
        return idPago;
    }

    public void setIdPago(Pago idPago) {
        this.idPago = idPago;
    }

  

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + this.id;
        hash = 71 * hash + Objects.hashCode(this.idPago);
        hash = 71 * hash + this.idEstacionamiento;
        hash = 71 * hash + Objects.hashCode(this.nomEstacionamiento);
        hash = 71 * hash + this.cantidad;
        hash = 71 * hash + this.numeroTick;
        hash = 71 * hash + this.montoP;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Detalle other = (Detalle) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.idEstacionamiento != other.idEstacionamiento) {
            return false;
        }
        if (this.cantidad != other.cantidad) {
            return false;
        }
        if (this.numeroTick != other.numeroTick) {
            return false;
        }
        if (this.montoP != other.montoP) {
            return false;
        }
        if (!Objects.equals(this.nomEstacionamiento, other.nomEstacionamiento)) {
            return false;
        }
        if (!Objects.equals(this.idPago, other.idPago)) {
            return false;
        }
        return true;
    }
    
    
}
