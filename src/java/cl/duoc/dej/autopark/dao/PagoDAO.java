/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dej.autopark.dao;

import cl.duoc.dej.autopark.model.Pago;
import java.util.List;

/**
 *
 * @author Raniela
 */
public interface PagoDAO extends BaseDAO<Pago> {
    
    @Override
    boolean agregar(Pago elemento);

    @Override
    boolean actualizar(Pago elemento);

    @Override
    boolean eliminar(Pago elemento);

    @Override
    Pago buscar(Pago elemento);

    @Override
    List<Pago> listar();
    
}
