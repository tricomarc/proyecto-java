/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dej.autopark.dao;

import cl.duoc.dej.autopark.model.Detalle;
import java.util.List;

/**
 *
 * @author Raniela
 */
public interface DetalleDAO extends BaseDAO<Detalle> {
    
    @Override
    boolean agregar(Detalle elemento);

    @Override
    boolean actualizar(Detalle elemento);

    @Override
    boolean eliminar(Detalle elemento);

    @Override
    Detalle buscar(Detalle elemento);

    @Override
    List<Detalle> listar();
    
    public List<Detalle> listarRut(int rut);
    
    public List<Detalle> listarPorBucher(int dato);
    
    public List<Detalle> listarVoucher();
    
    public List<Detalle> listarVoucher2();
    
    
    
    
}
