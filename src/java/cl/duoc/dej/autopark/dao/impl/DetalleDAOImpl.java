/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dej.autopark.dao.impl;

import cl.duoc.dej.autopark.dao.DetalleDAO;
import cl.duoc.dej.autopark.model.Detalle;
import cl.duoc.dej.autopark.model.Pago;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.auth.message.callback.PrivateKeyCallback;


public class DetalleDAOImpl implements DetalleDAO {

    Connection connection;
    
    private static final Logger LOGGER = Logger.getLogger(DetalleDAOImpl.class.getName());
   
    
    // Se instancia conexion
    public DetalleDAOImpl(Connection connection){
    
        this.connection = connection;
       // LOGGER.info("realizando conexion");
    }

    
    @Override
    public boolean agregar(Detalle detalle) {
         boolean creo = false;
         
        try {

            String querySQLInsertar = "INSERT INTO  detalle "
                    + "(id, cantidad, id_estacionamiento, numero_boucher,"
                    + "nombre_estacionamiento, monto_pago) "
                    + " VALUES (NULL,?,?,0,?,?) ";
            
            
            PreparedStatement stmt = this.connection.prepareStatement(querySQLInsertar);
            
            stmt.setInt(1, detalle.getCantidad());
            stmt.setInt(2, detalle.getIdEstacionamiento());
            stmt.setString(3, detalle.getNomEstacionamiento());
            stmt.setInt(4, detalle.getMontoP());
            
            stmt.executeUpdate();
            System.out.println("Agreego detalle sin n° Bouche0)");
            
            
            String updateNum = "UPDATE detalle SET numero_boucher = (SELECT numero_boucher " +
                                "FROM pago " +
                                "ORDER BY numero_boucher DESC " +
                                "LIMIT 1)" +
                                "where numero_boucher = 0";
            
            PreparedStatement stmt2 = this.connection.prepareStatement(updateNum);
            stmt2.executeUpdate();
            
            LOGGER.info("Se agrego detalle correctamente");
           
          //  System.out.println("Actualiza detalle con n° Bouche0)");
            // System.out.println("Agrego Detalle completo");
            creo = true;
        //    System.out.println("resultado Boolean " + creo);
            
        }  catch(SQLException e) {
            LOGGER.log(Level.SEVERE, "No se inserto detalle en BD: "+e.getMessage());
        }
        
    //    System.out.println("resultado Boolean " + creo);
        return creo;
         
    
    }

    @Override
    public boolean actualizar(Detalle elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eliminar(Detalle elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Detalle buscar(Detalle elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Detalle> listar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Detalle> listarRut(int rut) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        List<Detalle> lista = new ArrayList<>();
       String querySQl = "SELECT d.nombre_estacionamiento,"
               + " d.monto_pago,"
               + " p.rut_usuario,"
               + "p.numero_boucher"
               + " FROM detalle d JOIN pago p ON d.numero_boucher = p.numero_boucher"
               + " WHERE p.rut_usuario = ?";
       try{
//           Statement statement = this.connection.createStatement();
//           statement.execute(querySQl);
//           ResultSet resultSet = statement.getResultSet();

           PreparedStatement stmt = this.connection.prepareStatement(querySQl);
           stmt.setInt(1, rut);
           ResultSet resultSet = stmt.executeQuery();
           Pago  pago;
           Detalle detalle;
//           if ( stmt.execute(querySQl)) {
//               System.out.println("Lista");
//           }else{
//               System.out.println("No lista");
//           }
           while(resultSet.next()){
               detalle = new Detalle();
               pago = new Pago();
//               pago.setRutCliente(rut);
               
               detalle.setNomEstacionamiento(resultSet.getString("d.nombre_estacionamiento"));
               detalle.setMontoP(resultSet.getInt("d.monto_pago"));
               pago.setRutCliente(resultSet.getInt("p.rut_usuario"));
               pago.setNumeroBoucher(resultSet.getInt("p.numero_boucher"));
               detalle.setIdPago(pago);
               lista.add(detalle);
               
           }
            LOGGER.info("Se creo lista por rut con exito!");
           
       } catch(SQLException e) {
            LOGGER.log(Level.SEVERE, "No se creo lista por rut: "+e.getMessage());
       }
       return lista;
    }

    @Override
    public List<Detalle> listarPorBucher(int dato) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        List<Detalle> lista = new ArrayList<>();
       String querySQl = "SELECT d.nombre_estacionamiento,"
               + " d.monto_pago,"
               + " p.rut_usuario,"
               + "p.numero_boucher"
               + " FROM detalle d JOIN pago p ON d.numero_boucher = p.numero_boucher"
               + " WHERE p.numero_boucher = ?";
       try{
//           Statement statement = this.connection.createStatement();
//           statement.execute(querySQl);
//           ResultSet resultSet = statement.getResultSet();

           PreparedStatement stmt = this.connection.prepareStatement(querySQl);
           stmt.setInt(1, dato);
           ResultSet resultSet = stmt.executeQuery();
           Pago  pago;
           Detalle detalle;
//           if ( stmt.execute(querySQl)) {
//               System.out.println("Lista");
//           }else{
//               System.out.println("No lista");
//           }
           while(resultSet.next()){
               detalle = new Detalle();
               pago = new Pago();
//               pago.setRutCliente(rut);
               
               detalle.setNomEstacionamiento(resultSet.getString("d.nombre_estacionamiento"));
               detalle.setMontoP(resultSet.getInt("d.monto_pago"));
               pago.setRutCliente(resultSet.getInt("p.rut_usuario"));
               pago.setNumeroBoucher(resultSet.getInt("p.numero_boucher"));
               detalle.setIdPago(pago);
               lista.add(detalle);
               
           }
        LOGGER.info("Se creo lista por Boucher con exito!");
           
       } catch(SQLException e) {
            LOGGER.log(Level.SEVERE, "No se creo lista por Boucher: "+e.getMessage());
       }
       return lista;
    }
    
    
    @Override
    public List<Detalle> listarVoucher() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
       List<Detalle> lista = new ArrayList<>();
       String querySQL = "SELECT p.numero_boucher,"
               + "d.nombre_estacionamiento,"
               + "d.monto_pago,"
               + "d.id, d.cantidad,"
               + " p.total_pago,"
               + " p.opcion_envio FROM pago p JOIN detalle d ON p.numero_boucher = d.numero_boucher"
               + " WHERE p.numero_boucher = (SELECT numero_boucher "
               + "FROM pago ORDER BY numero_boucher "
               + "DESC LIMIT 1)";
       
        Statement statement;
        try {
            statement = this.connection.createStatement();
            statement.execute(querySQL);
            ResultSet resultSet = statement.getResultSet();
            Detalle detalle;
            Pago pago;
            while(resultSet.next()){
                detalle = new Detalle();
                pago = new Pago();
                
                pago.setNumeroBoucher(resultSet.getInt("p.numero_boucher"));
                detalle.setNomEstacionamiento(resultSet.getString("d.nombre_estacionamiento"));
                detalle.setMontoP(resultSet.getInt("d.monto_pago"));
                detalle.setCantidad(resultSet.getInt("d.cantidad"));
                detalle.setId(resultSet.getInt("d.id"));
                pago.setTotalPago(resultSet.getInt("p.total_pago"));
                pago.setOpcionEnvio(resultSet.getString("p.opcion_envio"));
                
                detalle.setIdPago(pago);
                lista.add(detalle);
            }
        LOGGER.info("Se creo lista por Voucher con exito!");
           
       } catch(SQLException e) {
            LOGGER.log(Level.SEVERE, "No se creo lista por Voucher: "+e.getMessage());
       }
        return lista;
        
    }
    
    // Entrega solo 1 dato
    @Override
    public List<Detalle> listarVoucher2() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
       List<Detalle> lista = new ArrayList<>();
       String querySQL = "SELECT p.numero_boucher, d.nombre_estacionamiento, d.monto_pago, d.id, p.total_pago, p.opcion_envio " +
                            "FROM pago p JOIN detalle d ON p.numero_boucher = d.numero_boucher " +
                            "WHERE p.numero_boucher = (SELECT numero_boucher " +
                                    "FROM pago " +
                                    "ORDER BY numero_boucher DESC LIMIT 1 )" +
                            "ORDER BY numero_boucher DESC LIMIT 1";
       
        Statement statement;
        try {
            statement = this.connection.createStatement();
            statement.execute(querySQL);
            ResultSet resultSet = statement.getResultSet();
            Detalle detalle;
            Pago pago;
            while(resultSet.next()){
                detalle = new Detalle();
                pago = new Pago();
                
                pago.setNumeroBoucher(resultSet.getInt("p.numero_boucher"));
                detalle.setNomEstacionamiento(resultSet.getString("d.nombre_estacionamiento"));
                detalle.setMontoP(resultSet.getInt("d.monto_pago"));
                detalle.setId(resultSet.getInt("d.id"));
                pago.setTotalPago(resultSet.getInt("p.total_pago"));
                pago.setOpcionEnvio(resultSet.getString("p.opcion_envio"));
                
                detalle.setIdPago(pago);
                lista.add(detalle);
            }
        LOGGER.info("Se creo lista por Voucher2 con exito!");
           
       } catch(SQLException e) {
            LOGGER.log(Level.SEVERE, "No se creo lista por Voucher2: "+e.getMessage());
       }
        return lista;
        
    }
    
    
}
