/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dej.autopark.dao.impl;

import cl.duoc.dej.autopark.dao.PagoDAO;
import cl.duoc.dej.autopark.model.Pago;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class PagoDAOImpl implements PagoDAO {

    
    Connection connection;
    
    private static final Logger LOGGER = Logger.getLogger(PagoDAOImpl.class.getName());
   
    
    // Se instancia conexion
    public PagoDAOImpl(Connection connection){
    
        this.connection = connection;
    }
    
    
    @Override
    public boolean agregar(Pago pago) {
        boolean creo = false;
        
        try {

            String querySQLInsertar = "INSERT INTO  pago "
                    + "(numero_boucher, total_pago, opcion_envio, rut_usuario, "
                    + "opcion_pago, nombreCl, email, telefono) "
                    + " VALUES (NULL,?,?,?,?,?,?,?)";
            
            PreparedStatement stmt = this.connection.prepareStatement(querySQLInsertar);
            
            stmt.setInt(1, pago.getTotalPago());
            stmt.setString(2, pago.getOpcionEnvio());
            stmt.setInt(3, pago.getRutCliente()); 
            stmt.setString(4, pago.getOpcionPago());
            stmt.setString(5, pago.getNombreCliente());
            stmt.setString(6, pago.getEmail());
            stmt.setInt(7, pago.getTelefono());
            
            stmt.executeUpdate();
            
            
         //   System.out.println("Agrego Pago");
            creo = true;
           // System.out.println("resultado Boolean " + creo);
            
            LOGGER.info("Se agrego pago correctamente");
           
        }  catch(SQLException e) {
            LOGGER.log(Level.SEVERE, "No se inserto pago en BD: "+e.getMessage());
        }
        
       // System.out.println("resultado Boolean " + creo);
        return creo;
        
    }

    @Override
    public boolean actualizar(Pago elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eliminar(Pago elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pago buscar(Pago elemento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Pago> listar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
