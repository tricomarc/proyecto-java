

package cl.duoc.dej.autopark.dao;

import java.util.List;


public interface BaseDAO<T> {
    //SE declara lo que se hara en la bd
    
    boolean agregar (T elemento);
    boolean actualizar(T elemento);
    boolean eliminar(T elemento);;
    T buscar (T elemento);;
    List<T> listar();
    
    
}
