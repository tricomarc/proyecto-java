/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dej.autopark.servlet;

import cl.duoc.dej.autopark.dao.DetalleDAO;
import cl.duoc.dej.autopark.dao.PagoDAO;
import cl.duoc.dej.autopark.dao.impl.DetalleDAOImpl;
import cl.duoc.dej.autopark.dao.impl.PagoDAOImpl;
import cl.duoc.dej.autopark.model.Detalle;
import cl.duoc.dej.autopark.model.Pago;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author IBMSEGURIDAD01
 */
public class InicioServlet extends HttpServlet {

    //Se instancia conexion a base de datos
    @Resource(name="jdbc/autoparkDS")
    //Recibe la conexión        
    DataSource dataSource;
    
    private final static Logger LOGGER = Logger.getLogger(InicioServlet.class.getName());
   
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.setAttribute("alertaList", false);
        
        request.getRequestDispatcher("/WEB-INF/jsp/publico/inicio.jsp").forward(request, response);
     
      
       
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        
        try {
        // Se realiza conexxion
        Connection connection = dataSource.getConnection();
                 
                 
         // Se declara los botones para rescatar sus valores
         String btn_agregar = request.getParameter("btn_agregar");
         String btn_pagar = request.getParameter("btn_pagar");
         String btn_quitar = request.getParameter("btn_quitar");
         
         
        if(btn_agregar!=null){
            // Solo carga la tabla y el monto referencial
            
            // Crea la lista y calcula montos (Vista previa)
            // <editor-fold>  
            
           
            // rescata el valor del combobox
            String cmb_estacionamiento = request.getParameter("cmb_estacionamiento");
         
            if(Integer.parseInt(cmb_estacionamiento)==0){
                request.setAttribute("alerta", "Debe seleccionar un estacionamiento");
            }else{
            
                 // Activa la lista
            request.setAttribute("alertaList", true);
                
            // Hacer la lista y que calcule el monto
            HttpSession sesion = request.getSession(true);
            ArrayList<Detalle> lista = (ArrayList<Detalle>)sesion.getAttribute("d_lista");
            
            // Se crea un objeto detalle
            Detalle d = new Detalle();

            // Si la lista es nueva
            if(lista == null)
            {
                lista = new ArrayList<Detalle>();
                
                // carga el objeto a la lista
                d.setCantidad(1); 
                d.setIdEstacionamiento(Integer.parseInt(cmb_estacionamiento));
                
                if(d.getIdEstacionamiento()==1){
                    d.setNomEstacionamiento("Maule");
                }else if(d.getIdEstacionamiento()==2){
                     d.setNomEstacionamiento("Castro");
                }else if(d.getIdEstacionamiento()==3){
                    d.setNomEstacionamiento("Puerto Montt");
                }
                lista.add(d);
                
                
                
                // Si la lista no es nueva
            }else { 
                // Recorre el arreglo
                    for (Detalle detalle : lista) {
                        
                        // Si se repite el codigo aumenta la cantidad       
                        if(detalle.getIdEstacionamiento()== Integer.parseInt(cmb_estacionamiento)){
                            System.out.println("Repetido");
                                  d.setCantidad(detalle.getCantidad()+1);
                                  lista.remove(detalle);
                                  d.setIdEstacionamiento(Integer.parseInt(cmb_estacionamiento));
                                  
                                  if(d.getIdEstacionamiento()==1){
                                        d.setNomEstacionamiento("Maule");
                                    }else if(d.getIdEstacionamiento()==2){
                                         d.setNomEstacionamiento("Castro");
                                    }else if(d.getIdEstacionamiento()==3){
                                        d.setNomEstacionamiento("Puerto Montt");
                                    }
                                  lista.add(d);  
                                 break;
                                 
                        // Si el codigo no se repite agrega un nuevo objeto a la lista
                        }else{
                            System.out.println("No repetido");
                                  d.setCantidad(1);
                                  d.setIdEstacionamiento(Integer.parseInt(cmb_estacionamiento));
                                  
                                    if(d.getIdEstacionamiento()==1){
                                        d.setNomEstacionamiento("Maule");
                                    }else if(d.getIdEstacionamiento()==2){
                                         d.setNomEstacionamiento("Castro");
                                    }else if(d.getIdEstacionamiento()==3){
                                        d.setNomEstacionamiento("Puerto Montt");
                                    }
                                    
                                  lista.add(d); 
                                 break;
                        }
                    }
            }

            
            // Calculo para sacar monto a pagar
            int montoPagar = 0;
           // int cantTotal =0;
            int cantTotalE1 = 0;
            int cantTotalE2 = 0;
            int cantTotalE3 = 0;
            
            
            for (Detalle detalle : lista) {
                
                if(detalle.getIdEstacionamiento()==1){
                    cantTotalE1 = cantTotalE1 + detalle.getCantidad();
                //    System.out.println("Cantidad 1 " + cantTotalE1);
                    detalle.setMontoP(cantTotalE1 * 1500);
                    
                }else if(detalle.getIdEstacionamiento()==2){
                    cantTotalE2 = cantTotalE2 + detalle.getCantidad();
               //     System.out.println("Cantidad 2 " + cantTotalE2);
                    detalle.setMontoP(cantTotalE2 * 2000);
                    
                }else if(detalle.getIdEstacionamiento()==3){
                    cantTotalE3 = cantTotalE3 + detalle.getCantidad();
                    detalle.setMontoP(cantTotalE3 * 2500);
                //    System.out.println("Cantidad 3 " + cantTotalE3);
                }
                    
            }
            
                cantTotalE1 = cantTotalE1 *1500;
             //   System.out.println("Cantidad a pagar 1:  " + cantTotalE1);
                cantTotalE2 = cantTotalE2 *2000;
              //  System.out.println("Cantidad a pagar 2:  " + cantTotalE2);
                cantTotalE3 = cantTotalE3 *2500;
           //     System.out.println("Cantidad a pagar 3:  " + cantTotalE3);
               // cantTotal = cantTotal + detalle.getCantidad();
                montoPagar = cantTotalE1 +  cantTotalE2 + cantTotalE3; 
            
            
         //   System.out.println("Monto a pagar " + montoPagar);
            sesion.setAttribute("d_lista", lista);
            sesion.setAttribute("monto", montoPagar);
             }
            
            request.getRequestDispatcher("/WEB-INF/jsp/publico/inicio.jsp").forward(request, response);
            
           // </editor-fold>
           // Termino
           
           
         }
             
        if(btn_pagar!=null){
            
            // <editor-fold>  
            
            // Se rescatan valores del formulario ingresados por usuario
            String txt_rut = request.getParameter("txt_rut");
            String txt_nombre = request.getParameter("txt_nombre");
            String txt_telefono = request.getParameter("txt_telefono");
            String txt_email = request.getParameter("txt_email");
            String RbOpPago = request.getParameter("RbOpPago");
            String RbOpEnvio = request.getParameter("RbOpEnvio");
       //     String cmb_estacionamiento = request.getParameter("cmb_estacionamiento");
            String lblMontoP = request.getParameter("lblMontoP");

            
//            // imprime en consola los datos rescatados
//                System.out.println("Rut: "+txt_rut + 
//                        "- Nombre "+ txt_nombre+ 
//                        " - Telefono "+  txt_telefono + 
//                        " - Email "+ txt_email+ 
//                        " - Pago " + RbOpPago +
//                        " - Envio " + RbOpEnvio);
//          
            
            // Validaciones d edatos
            // <editor-fold>  
           
                     
            // Carga a la BD
            // <editor-fold>  
            // Se crean a los dao
                PagoDAO pdao = new PagoDAOImpl(connection);
                DetalleDAO ddao = new DetalleDAOImpl(connection);

                // Se crean los objetos
                Pago pago = new Pago();
               
                 pago.setNumeroBoucher(0);
                 pago.setOpcionPago(RbOpPago);
                 pago.setOpcionEnvio(RbOpEnvio);
                 pago.setRutCliente(Integer.parseInt(txt_rut));
                 pago.setNombreCliente(txt_nombre);
                 pago.setEmail(txt_email);
                 pago.setTelefono(Integer.parseInt(txt_telefono));
                 
                 pago.setTotalPago(Integer.parseInt(lblMontoP));
                 
                 if(pdao.agregar(pago)){
                  //  System.out.println("Se agrego pago");
                    LOGGER.info("Se agrega Pago en BD");
                 }else{
                    LOGGER.info("No se agrega Pago en BD");
                        request.setAttribute("alertap", "No se agrego pago, codigo ya esta creado o datos ingresados estan erroneos");
                 }
                 
                  // Se agregan datos a objeto detalle
                 Detalle detallen = new Detalle();
                 
                HttpSession sesion = request.getSession(true);
                ArrayList<Detalle> lista = (ArrayList<Detalle>)sesion.getAttribute("d_lista");
                
                for (Detalle detalle1 : lista) {
                   
                        detallen.setIdPago(detalle1.getIdPago());
                        detallen.setIdEstacionamiento(detalle1.getIdEstacionamiento());
                        detallen.setNomEstacionamiento(detalle1.getNomEstacionamiento());
                        detallen.setCantidad(detalle1.getCantidad());
                        detallen.setMontoP(detalle1.getMontoP());
                        
                        if(ddao.agregar(detallen)){
                            System.out.println("Se agrego detalle");
                            LOGGER.info("Se agrega detalle en BD");
                        }else{
                            System.out.println("No agrego detalle");
                            LOGGER.info("No se agrega detalle en BD");
                            request.setAttribute("alertad", "No se agrego detalle, codigo ya esta creado o datos ingresados estan erroneos");
                        }
                  } 
                    List<Detalle> detalle = ddao.listarVoucher(); 
                    request.setAttribute("listab", detalle);

                    List<Detalle> detalle2 = ddao.listarVoucher2(); 
                    request.setAttribute("listab2", detalle2);

        
                    // </editor-fold>
                    
                    // </editor-fold>
                    
                    
                    
                    
                    request.getRequestDispatcher("/WEB-INF/jsp/publico/voucher.jsp").forward(request, response);
                
                // </editor-fold>
           // Termino
               
        } 
        
            if(btn_quitar!=null){

                // <editor-fold> 
                int txtIdEst = Integer.parseInt(request.getParameter("txtIdEst"));
                System.out.println("BTQ: " +  btn_quitar);
                System.out.println("VAlor ID: " +  txtIdEst);

                // Activa la lista
                request.setAttribute("alertaList", true);

                // Hacer la lista y que calcule el monto
                HttpSession sesion = request.getSession(true);
                ArrayList<Detalle> lista = (ArrayList<Detalle>)sesion.getAttribute("d_lista");

                // Se crea un objeto detalle
                Detalle d = new Detalle();

                for (Detalle detalle : lista) {

                            // Si se repite el codigo aumenta la cantidad       
                            if(detalle.getIdEstacionamiento()== txtIdEst){
                              //  System.out.println("Repetido");

                                      if(detalle.getCantidad()-1>0){
                                          d.setCantidad(detalle.getCantidad()-1);
                                            d.setIdEstacionamiento(txtIdEst);
                                        if(d.getIdEstacionamiento()==1){
                                            d.setNomEstacionamiento("Maule");
                                        }else if(d.getIdEstacionamiento()==2){
                                            d.setNomEstacionamiento("Castro");
                                        }else if(d.getIdEstacionamiento()==3){
                                            d.setNomEstacionamiento("Puerto Montt");
                                        } 
                                        LOGGER.info("Se agrega detalle");
                                            lista.add(d);  

                                      }
                                      lista.remove(detalle);
                                      break;

                            // Si el codigo no se repite agrega un nuevo objeto a la lista
                            }else{
                                System.out.println("No repetido");
                                break;
                            }
                }

                // Calculo para sacar monto a pagar
                int montoPagar = 0;
               // int cantTotal =0;
                int cantTotalE1 = 0;
                int cantTotalE2 = 0;
                int cantTotalE3 = 0;


                for (Detalle detalle : lista) {

                    if(detalle.getIdEstacionamiento()==1){
                        cantTotalE1 = cantTotalE1 + detalle.getCantidad();
                    //    System.out.println("Cantidad 1 " + cantTotalE1);
                        detalle.setMontoP(cantTotalE1 * 1500);

                    }else if(detalle.getIdEstacionamiento()==2){
                        cantTotalE2 = cantTotalE2 + detalle.getCantidad();
                   //     System.out.println("Cantidad 2 " + cantTotalE2);
                        detalle.setMontoP(cantTotalE2 * 2000);

                    }else if(detalle.getIdEstacionamiento()==3){
                        cantTotalE3 = cantTotalE3 + detalle.getCantidad();
                        detalle.setMontoP(cantTotalE3 * 2500);
                    //    System.out.println("Cantidad 3 " + cantTotalE3);
                    }

                }

                    cantTotalE1 = cantTotalE1 *1500;
                 //   System.out.println("Cantidad a pagar 1:  " + cantTotalE1);
                    cantTotalE2 = cantTotalE2 *2000;
                  //  System.out.println("Cantidad a pagar 2:  " + cantTotalE2);
                    cantTotalE3 = cantTotalE3 *2500;
               //     System.out.println("Cantidad a pagar 3:  " + cantTotalE3);
                   // cantTotal = cantTotal + detalle.getCantidad();
                    montoPagar = cantTotalE1 +  cantTotalE2 + cantTotalE3; 


             //   System.out.println("Monto a pagar " + montoPagar);
                sesion.setAttribute("d_lista", lista);
                sesion.setAttribute("monto", montoPagar);

                // </editor-fold> 
                request.getRequestDispatcher("/WEB-INF/jsp/publico/inicio.jsp").forward(request, response);

            }
        
        } catch (Exception e) {
           request.setAttribute("error", "Error En la consulta de Agregar,codigo ya esta creado o datos ingresados estan erroneos" );
           LOGGER.log(Level.SEVERE, "Error al procesar la solicitud Inicio Servlet: "+e.getMessage());
           request.getRequestDispatcher("/WEB-INF/jsp/publico/inicio.jsp").forward(request, response);
        }
              
    }
    

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
