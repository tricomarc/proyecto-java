/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.dej.autopark.servlet;

import cl.duoc.dej.autopark.dao.DetalleDAO;
import cl.duoc.dej.autopark.dao.impl.DetalleDAOImpl;
import cl.duoc.dej.autopark.model.Detalle;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 *
 * @author mlazo
 */
public class VoucherServlet extends HttpServlet {
    
    @Resource(name="jdbc/autoparkDS")
    DataSource dataSource;
       
    private final static Logger LOGGER = Logger.getLogger(InicioServlet.class.getName());
   

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            Connection connection = dataSource.getConnection();
            DetalleDAO dao = new DetalleDAOImpl(connection);
           

                    List<Detalle> detalle = dao.listarVoucher(); 
                    request.setAttribute("listab", detalle);
                    
                    List<Detalle> detalle2 = dao.listarVoucher2(); 
                    request.setAttribute("listab2", detalle2);
                    
                     LOGGER.info("Se genero o mostro boucher");
                                       
            
             request.getRequestDispatcher("/WEB-INF/jsp/publico/voucher.jsp").forward(request, response);
        } catch (Exception e) {
          //  request.setAttribute("error", "Error En la consulta de Agregar,codigo ya esta creado o datos ingresados estan erroneos" );
           LOGGER.log(Level.SEVERE, "Error al procesar la solicitud Voucher Servlet: "+e.getMessage());
           request.getRequestDispatcher("/WEB-INF/jsp/publico/inicio.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         
        request.getRequestDispatcher("/WEB-INF/jsp/publico/voucher.jsp").forward(request, response);
     
      
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
